#!/bin/bash
if [ "$1" = "build-web" ]
then
	rm -rf ./dist && mkdir ./dist
	svn co https://app.rpidev.org/svnroot/ilab/web/trunk /tmp/ilab
	pushd /tmp/ilab
	yarn install && yarn build
	popd
	cp -r /tmp/ilab/dist/* ./dist
fi

if [ "$2" = "image" ]
then 
	docker-compose build api
fi

docker-compose down

docker-compose up -d