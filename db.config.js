module.exports = {
  database: "ilab",
  host: process.env.DBHOST || "127.0.0.1",
  port: process.env.DBPORT || 3307,
  user: process.env.DBUSER || "root",
  password: process.env.DBPASS || "123456"
};
