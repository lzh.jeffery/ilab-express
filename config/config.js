const dbConfig = require("../db.config");

module.exports = {
  development: {
    username: dbConfig.user,
    password: dbConfig.password,
    dialect: "mysql",
    host: dbConfig.host,
    database: dbConfig.database,
    port: `${dbConfig.port}`
  },
  production: {
    username: dbConfig.user,
    password: dbConfig.password,
    dialect: "mysql",
    host: dbConfig.host,
    database: dbConfig.database,
    port: `${dbConfig.port}`
  },
  test: {
    username: dbConfig.user,
    password: dbConfig.password,
    dialect: "mysql",
    host: dbConfig.host,
    database: dbConfig.database
  }
};
