const lab = DataTypes => {
  return {
    name: "lab",
    table: "labs",
    extra: {
      timestamps: false
    },
    associations: [
      {
        with: "question",
        type: "hasMany",
        extra: {
          as: "questions",
          foreignKey: "lab_id"
        }
      }
    ],
    fields: {
      id: {
        allowNull: false,
        autoIncrement: true,
        type: DataTypes.INTEGER,
        primaryKey: true
      },
      name: {
        allowNull: false,
        type: DataTypes.STRING(512)
      },
      vrScore: {
        allowNull: false,
        type: DataTypes.INTEGER,
        defaultValues: 0
      },
      video: {
        allowNull: true,
        type: DataTypes.STRING(512),
        defaultValues: null
      }
    }
  };
};

const QUESTION_TYPES = ["Radio", "Multiple Selection", "Open Question"];

const question = DataTypes => {
  return {
    name: "question",
    table: "questions",
    extra: {
      timestamps: false
    },
    dependencies: ["lab"],
    associations: [
      {
        with: "choice",
        type: "hasMany",
        extra: {
          as: "choices",
          foreignKey: "question_id"
        }
      },
      {
        with: "choice",
        type: "belongsToMany",
        extra: {
          as: "answers",
          through: "questionanswers",
          foreignKey: "question_id"
        }
      }
    ],
    fields: {
      id: {
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.INTEGER
      },
      lab_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
        references: {
          model: "labs",
          key: "id"
        },
        onDelete: "CASCADE"
      },
      text: {
        allowNull: false,
        type: DataTypes.TEXT
      },
      seq: {
        allowNull: false,
        type: DataTypes.INTEGER
      },
      type: {
        allowNull: false,
        type: DataTypes.STRING(64),
        validate: {
          isIn: {
            args: [QUESTION_TYPES],
            msg: `Must be ${QUESTION_TYPES.join(", ")}`
          }
        }
      },
      score: {
        allowNull: false,
        type: DataTypes.FLOAT
      },
      step: {
        allowNull: false,
        type: DataTypes.INTEGER,
        defaultValues: 1
      }
    }
  };
};

const choice = DataTypes => {
  return {
    name: "choice",
    table: "choices",
    extra: {
      timestamps: false
    },
    dependencies: ["question"],
    associations: [
      {
        with: "question",
        type: "belongsToMany",
        extra: {
          through: "questionanswers",
          foreignKey: "choice_id"
        }
      }
    ],
    fields: {
      id: {
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.INTEGER
      },
      question_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "questions",
          key: "id"
        },
        onDelete: "CASCADE"
      },
      seq: {
        allowNull: false,
        type: DataTypes.INTEGER
      },
      text: {
        allowNull: false,
        type: DataTypes.TEXT
      },
      label: {
        allowNull: false,
        type: DataTypes.STRING("64")
      }
    }
  };
};

const questionanswer = DataTypes => {
  return {
    name: "questionanswers",
    table: "questionanswers",
    extra: {
      timestamps: false
    },
    dependencies: ["question", "choice"],
    fields: {
      id: {
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
        type: DataTypes.INTEGER
      },
      question_id: {
        type: DataTypes.INTEGER,
        references: {
          model: "questions",
          key: "id"
        },
        onDelete: "CASCADE"
      },
      choice_id: {
        type: DataTypes.INTEGER,
        references: {
          model: "choices",
          key: "id"
        },
        onDelete: "CASCADE"
      }
    }
  };
};

const answersheet = DataTypes => {
  return {
    name: "answersheet",
    table: "answersheets",
    extra: {
      timestamps: true
    },
    dependencies: ["lab", "account", "lesson"],
    associations: [
      {
        with: "answersheetitem",
        type: "hasMany",
        extra: {
          foreignKey: "answersheet_id",
          as: "answers"
        }
      },
      {
        with: "account",
        type: "belongsTo",
        extra: {
          foreignKey: "account_id",
          as: "account"
        }
      },
      {
        with: "lab",
        type: "belongsTo",
        extra: {
          foreignKey: "lab_id",
          as: "lab"
        }
      }
    ],
    fields: {
      id: {
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.INTEGER
      },
      lab_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
        references: {
          model: "labs",
          key: "id"
        },
        onDelete: "CASCADE"
      },
      account_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        references: {
          model: "accounts",
          key: "id"
        },
        onDelete: "SET NULL"
      },
      lesson_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        references: {
          model: "lessons",
          key: "id"
        },
        onDelete: "SET NULL"
      },
      total: {
        type: DataTypes.FLOAT,
        allowNull: false
      },
      score: {
        type: DataTypes.FLOAT,
        allowNull: false
      },
      isEnd: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        default: false
      }
    }
  };
};

const ANSWER_SHEET_ITEM_TYPES = ["choice", "text"];

const answersheetitem = DataTypes => {
  return {
    name: "answersheetitem",
    table: "answersheetitems",
    extra: {
      timestamps: false
    },
    associations: [
      {
        with: "question",
        type: "belongsTo",
        extra: {
          foreignKey: "question_id",
          as: "question"
        }
      }
    ],
    dependencies: ["question", "answersheet"],
    fields: {
      id: {
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.INTEGER
      },
      answersheet_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
        references: {
          model: "answersheets",
          key: "id"
        },
        onDelete: "CASCADE"
      },
      question_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
        references: {
          model: "questions",
          key: "id"
        },
        onDelete: "CASCADE"
      },
      type: {
        type: DataTypes.STRING(128),
        allowNull: false,
        validate: {
          isIn: {
            args: [ANSWER_SHEET_ITEM_TYPES],
            msg: `Must be ${ANSWER_SHEET_ITEM_TYPES.join(", ")}`
          }
        }
      },
      score: {
        type: DataTypes.FLOAT,
        allowNull: false
      },
      data: {
        type: DataTypes.TEXT,
        allowNull: false
      },
      comment: {
        type: DataTypes.TEXT,
        allowNull: true,
        defaultValues: null
      }
    }
  };
};

// const answersheetaudioitem = DataTypes => {
//   return {
//     name: "answersheetaudioitem",
//     table: "answersheetaudioitems",
//     extra: { timestamps: false },
//     associations: [
//       {
//         with: "file",
//         type: "belongsTo",
//         extra: {
//           foreignKey: "file_id",
//           targetKey: "id",
//           as: "audios"
//         }
//       }
//     ],
//     fields: {
//       id: {
//         allowNull: false,
//         autoIncrement: true,
//         primaryKey: true,
//         type: DataTypes.INTEGER
//       },
//       answersheet_id: {
//         allowNull: false,
//         type: DataTypes.INTEGER,
//         foreignKey: {
//           model: "answersheetitems",
//           key: "id"
//         },
//         onDelete: "CASCADE"
//       },
//       file_id: {
//         allowNull: false,
//         type: DataTypes.INTEGER,
//         foreignKey: {
//           model: "files",
//           key: "id"
//         },
//         onDelete: "CASCADE"
//       },
//       seq: {
//         allowNull: false,
//         type: DataTypes.INTEGER
//       }
//     }
//   };
// };

const account = DataTypes => {
  return {
    name: "account",
    table: "accounts",
    extra: {
      timestamps: true
    },
    dependencies: ["lesson"],
    associations: [
      {
        with: "role",
        type: "belongsToMany",
        extra: {
          through: "rolemappings",
          foreignKey: "account_id",
          as: "roles"
        }
      },
      {
        with: "lesson",
        type: "belongsTo",
        extra: {
          foreignKey: "lesson_id",
          targetKey: "id",
          as: "lesson"
        }
      }
    ],
    fields: {
      id: {
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.INTEGER
      },
      username: {
        type: DataTypes.STRING(512),
        allowNull: false
      },
      password: {
        type: DataTypes.STRING(256),
        allowNull: false
      },
      phone: {
        type: DataTypes.STRING(64),
        allowNull: false,
        unique: true
      },
      isVerified: {
        allowNull: true,
        type: DataTypes.BOOLEAN,
        defaultValues: null
      },
      identity: {
        allowNull: false,
        type: DataTypes.STRING(256)
      },
      name: {
        allowNull: false,
        type: DataTypes.STRING(256)
      },
      lesson_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        references: {
          model: "lessons",
          key: "id"
        },
        onDelete: "SET NULL",
        defaultValues: null
      }
    }
  };
};

const ROLES = ["SYSTEM_ADMIN", "TEACHER", "STUDENT"];
const role = DataTypes => {
  return {
    name: "role",
    table: "roles",
    extra: {
      timestamps: false
    },
    associations: [
      {
        with: "account",
        type: "belongsToMany",
        extra: {
          through: "rolemappings",
          foreignKey: "role_id",
          as: "accounts"
        }
      }
    ],
    fields: {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      name: {
        allowNull: false,
        type: DataTypes.STRING(128),
        validate: {
          isIn: {
            args: [ROLES],
            msg: `Must be ${ROLES.join(", ")}`
          }
        }
      }
    }
  };
};

const rolemappings = DataTypes => {
  return {
    name: "rolemappings",
    table: "rolemappings",
    extra: {
      timestamps: false,
      uniqueKeys: {
        accountRole: { fields: ["account_id", "role_id"], customIndex: true }
      }
    },
    dependencies: ["role", "account"],
    fields: {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      account_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
        references: {
          model: "accounts",
          key: "id"
        },
        onDelete: "CASCADE",
        unique: "accountRole"
      },
      role_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
        references: {
          model: "roles",
          key: "id"
        },
        onDelete: "CASCADE",
        unique: "accountRole"
      }
    }
  };
};

const lesson = DataTypes => {
  return {
    name: "lesson",
    table: "lessons",
    extra: {
      timestamps: false
    },
    fields: {
      id: {
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.INTEGER
      },
      name: {
        allowNull: false,
        type: DataTypes.STRING(256)
      },
      isActive: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        defaultValues: true
      }
    }
  };
};

const file = DataTypes => {
  return {
    name: "file",
    table: "files",
    extra: {
      timestamps: false
    },
    fields: {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      type: {
        allowNull: false,
        type: DataTypes.STRING(128),
        validate: {
          isIn: [["file", "audio", "video"]],
          msg: "Must be file, audio or video"
        },
        defaultValue: "file"
      },
      url: {
        allowNull: false,
        type: DataTypes.STRING(512)
      },
      hash: {
        allowNull: false,
        type: DataTypes.STRING(512)
      }
    }
  };
};

module.exports.lab = lab;
module.exports.question = question;
module.exports.choice = choice;
module.exports.questionanswer = questionanswer;
module.exports.account = account;
module.exports.QUESTION_TYPES = QUESTION_TYPES;
module.exports.rolemappings = rolemappings;
module.exports.role = role;
module.exports.ROLES = ROLES;
module.exports.answersheet = answersheet;
module.exports.answersheetitem = answersheetitem;
module.exports.ANSWER_SHEET_ITEM_TYPES = ANSWER_SHEET_ITEM_TYPES;
module.exports.lesson = lesson;
module.exports.file = file;
// module.exports.answersheetaudioitem = answersheetaudioitem;
