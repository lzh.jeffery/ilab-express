const Mock = require("mockjs");
const { QUESTION_TYPES } = require("../models");
const bcrypt = require("bcrypt");

const labs = [
  {
    id: 1,
    name: "测试实验",
    vrScore: 40,
    video: "/vr/lab/1/iframe.html"
  }
];

let questions = [];
for (let i = 0; i < 20; i++) {
  let type;
  let text;
  switch (true) {
    case i < 10:
      type = QUESTION_TYPES[0];
      text = Mock.Random.csentence();
      break;
    case i >= 10 && i <= 17:
      type = QUESTION_TYPES[1];
      text = Mock.Random.cparagraph();
      break;
    default:
      type = QUESTION_TYPES[2];
      text = Mock.Random.cparagraph();
  }
  questions.push({
    id: i + 1,
    type,
    lab_id: 1,
    seq: i + 1,
    text,
    score: Mock.Random.integer(1, 10)
  });
}

let choices = [];
for (let i = 0; i < questions.length; i++) {
  let _question = questions[i];
  if (_question.type === QUESTION_TYPES[2]) continue;
  choices.push(
    ...["A", "B", "C", "D"].map((label, idx) => {
      return {
        id: i * 4 + idx + 1,
        question_id: questions[i].id,
        seq: idx + 1,
        text: `选项${questions[i].seq}-${idx + 1}`,
        label
      };
    })
  );
}

let accounts = [];
for (let i = 0; i <= 17; i++) {
  accounts.push({
    id: 4 + i,
    name: Mock.Random.cname(),
    username: Mock.Random.name(),
    identity: parseInt(Math.random() * 1000000000000000, 10)
      .toString()
      .substring(0, 18),
    password: bcrypt.hashSync("123456", 10),
    phone: parseInt(Math.random() * 1000000000000000, 10)
      .toString()
      .substring(0, 11),
    isVerified: [true, false, null][Mock.Random.integer(0, 2)],
    lesson_id: [1, 2, 3, null][Mock.Random.integer(0, 3)],
    createdAt: new Date(Mock.Random.date()),
    updatedAt: new Date(Mock.Random.date())
  });
}
accounts.push(
  ...[
    {
      id: 1,
      username: "sys",
      name: "system_admin",
      identity: "ROOT",
      password: bcrypt.hashSync("123456", 10),
      isVerified: true,
      phone: "00000000000",
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      id: 2,
      username: "teacher",
      name: "teacher",
      identity: "TEACHER",
      password: bcrypt.hashSync("123456", 10),
      isVerified: true,
      phone: "00000000001",
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      id: 3,
      username: "student",
      name: "student",
      identity: "STUDENT",
      password: bcrypt.hashSync("123456", 10),
      isVerified: true,
      phone: "00000000002",
      createdAt: new Date(),
      updatedAt: new Date()
    }
  ]
);

const questionanswers = [];
for (let i = 0; i < questions.length; i++) {
  let _question = questions[i];
  let _choices = choices.filter(cho => cho.question_id === _question.id);
  switch (_question.type) {
    case QUESTION_TYPES[0]:
      questionanswers.push({
        id: questionanswers.length + 1,
        question_id: _question.id,
        choice_id: _choices[Mock.Random.integer(0, _choices.length - 1)].id
      });
      break;
    case QUESTION_TYPES[1]:
      let answerNum = Mock.Random.integer(1, _choices.length);
      for (let j = 0; j < answerNum; j++) {
        let idx = Mock.Random.integer(0, _choices.length - 1);
        questionanswers.push({
          id: questionanswers.length + 1,
          question_id: _question.id,
          choice_id: _choices[idx].id
        });
        _choices.slice(idx, 1);
      }
      break;
    case QUESTION_TYPES[2]:
      break;
  }
}

module.exports = {
  labs,
  questions,
  accounts,
  choices,
  questionanswers
};
