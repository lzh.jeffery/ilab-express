"use strict";

const { ROLES } = require("../models");
let _labs = [];
let _questions = [
  {
    id: 1,
    lab_id: 1,
    text:
      "70年来，一批批像冯秉铨一样的归国知识分子为国家的建设、改革做出了巨大的贡献。在欧美同学会成立100周年庆祝大会上的讲话中，习近平说：“希望广大留学人员继承和发扬留学报国的光荣传统，做爱国主义的坚守者和传播者，秉持‘先天下之忧而忧，后天下之乐而乐’的人生理想，始终把国家富强、民族振兴、人民幸福作为努力志向，自觉使个人成功的果实结在爱国主义这颗常青树上。”个人成功的果实之所以应该结在爱国主义这棵常青树上，是因为爱国主义是",
    seq: 1,
    type: "Radio",
    step: 1,
    score: 5
  },
  {
    id: 2,
    lab_id: 1,
    text:
      "孙中山先生曾激励广大青年：要立志做大事，不要立志做大官。周恩来在中学时就立下了“为中华之崛起而读书”的志向；冯秉铨、李四光、邓稼先等老一辈知识分子，青年时期就立志用自己的聪明才智报效祖国。这些鲜活的实例给我们的启示是",
    seq: 2,
    score: 5,
    type: "Multiple Selection",
    step: 1
  },
  {
    id: 3,
    lab_id: 1,
    text:
      "冯秉铨先生少年时即立下科学报国的志向，这一志向对他后来的人生产生了重要的影响。我国许多古人也讲过立志对于人生重要意义，如墨子的“志不强智智不达”、王守仁的“志不立，天下无可成之事”等，这里的“志”实际上就是理想信念。“志”",
    seq: 3,
    type: "Multiple Selection",
    step: 1,
    score: 5
  },
  {
    id: 4,
    lab_id: 1,
    text:
      "我们今天的成就，离不开一代代爱国青年的不懈奋斗。谈谈你对青年与国家、社会关系的认识。",
    seq: 4,
    score: 9,
    step: 1,
    type: "Open Question"
  },
  {
    id: 5,
    lab_id: 1,
    text:
      "冯秉铨热爱国家的情感和建设国家的志向，对我们在新时期弘扬爱国主义精神有哪些启示？",
    seq: 5,
    score: 6,
    step: 1,
    type: "Open Question"
  },
  {
    id: 6,
    lab_id: 1,
    text:
      "冯秉铨认为作为知识分子，没有把著书立说作为最大的追求，而是将教育事业视为人生的意义，把培养人才奉献社会当做快乐。这说明评价人生价值的根本尺度是",
    type: "Radio",
    seq: 6,
    score: 5,
    step: 2
  },
  {
    id: 7,
    lab_id: 1,
    text:
      "爱因斯坦认为成就科学家的是品格而非才智。冯秉铨先生的人生，就很好的印证了这一观点。下列我国的传统名言中，具有同样含义的是",
    type: "Radio",
    seq: 7,
    score: 5,
    step: 2
  },
  {
    id: 8,
    lab_id: 1,
    text:
      "冯秉铨先生是一个当之无愧的奋斗者，他有强烈的责任感，不畏困难，长年承担着着常人难以想象的工作量，依然不改对教育的热爱，将之视为快乐。无论是现在的华工还是今天中国的无线电电子事业，都离不开冯秉铨的努力。从冯先生的经历可以体会到，关于奋斗、奋斗者和幸福的关系，以下说法正确的有",
    type: "Multiple Selection",
    step: 2,
    seq: 8,
    score: 5
  },
  {
    id: 9,
    lab_id: 1,
    text:
      "冯秉铨先生不拘泥于照搬苏联的教育模式，而是把教学工作当做一项科学研究来对待，结合实际积极推进教学改革，其具有的改革创新精神，是我们时代精神的核心。结合冯先生的经历和自身感受，你对改革创新、时代精神有怎样的理解？",
    type: "Open Question",
    seq: 9,
    score: 8,
    step: 2
  },
  {
    id: 10,
    lab_id: 1,
    text:
      "下面几张图片展示了建国以来中国GDP的变化，结合冯先生的讲述和你自身感受到的中国的发展变化，谈谈你的启示。",
    type: "Open Question",
    seq: 10,
    score: 6,
    step: 2
  }
];
let _accounts = [];
let _choices = [];
let _questionanswers = [];
let _lessons = [
  {
    id: 1,
    name: "思政1班",
    isActive: true
  },
  {
    id: 2,
    name: "思政2班",
    isActive: true
  },
  {
    id: 3,
    name: "思政3班",
    isActive: true
  }
];

let choicesObj = {
  1: {
    sure: [0],
    items: [
      "A.个人实现人生价值的力量源泉",
      "B.人实现人生价值的直接条件 ",
      "C.个人成功的根本保障 ",
      "D.个人成功的决定性因素"
    ]
  },
  2: {
    sure: [0, 1, 3],
    items: [
      "A.个人理想只有同国家的前途、民族的命运相结合才是有意义的",
      "B.个人的向往和追求要同社会的需要和人民的利益相一致",
      "C.社会理想和个人理想是矛盾的",
      "D.社会理想不是虚幻和空洞的，它体现在每一个社会成员的现实生活中"
    ]
  },
  3: {
    sure: [1, 2, 3],
    items: [
      "A.是人生价值评价标准",
      "B.是事业和人生的灯塔",
      "C.决定我们的立场和方向",
      "D.决定我们的言论和行动"
    ]
  },
  6: {
    sure: [2],
    items: [
      "A.个体在社会中的地位",
      "B.个体在社会中的影响",
      "C.个体对社会和他人的生存和发展的贡献",
      "D.个体从社会获得的满足程度"
    ]
  },
  7: {
    sure: [1],
    items: [
      "A.道虽迩，不行不至；事虽小，不为不成",
      "B.才者，德之资也；德者，才之帅也",
      "C.不学礼，无以立",
      "D.是非之心，智也"
    ]
  },
  8: {
    sure: [0, 1, 2],
    items: [
      "A.幸福都是奋斗出来的",
      "B.奋斗本身就是一种幸福",
      "C.奋斗者是最懂得幸福、最享受幸福的人",
      "D.奋斗者的幸福主要源于物质生活的富足"
    ]
  }
};

for (let key in choicesObj) {
  let obj = choicesObj[key];
  for (let i = 0; i < obj.items.length; i++) {
    let choice = obj.items[i];
    let [label, text] = choice.split(".");
    let _choice = {
      id: _choices.length + 1,
      question_id: key,
      seq: (_choices.length + 1) % 4 === 0 ? 4 : (_choices.length + 1) % 4,
      text,
      label
    };
    _choices.push(_choice);
    if (obj.sure.indexOf(i) !== -1) {
      _questionanswers.push({
        id: _questionanswers.length + 1,
        question_id: key,
        choice_id: _choices.length
      });
    }
  }
}

if (process.env.NODE_ENV === "development") {
  const {
    labs,
    // questions,
    accounts
    // choices,
    // questionanswers
  } = require("../mocks");
  _labs = labs;
  // _questions = questions;
  _accounts = accounts;
  // _choices = choices;
  // _questionanswers = questionanswers;
}

const roles = ROLES.map((role, index) => {
  return {
    id: index + 1,
    name: role
  };
});

const rolemappings = _accounts.map((act, index) => {
  return {
    id: index + 1,
    account_id: act.id,
    role_id: act.id > 2 ? 3 : act.id
  };
});

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async () => {
      try {
        await queryInterface.bulkInsert("lessons", _lessons, {});
        await queryInterface.bulkInsert("roles", roles, {});
        await queryInterface.bulkInsert("labs", _labs, {});
        await queryInterface.bulkInsert("questions", _questions, {});
        await queryInterface.bulkInsert("accounts", _accounts, {});
        await queryInterface.bulkInsert("choices", _choices, {});
        await queryInterface.bulkInsert(
          "questionanswers",
          _questionanswers,
          {}
        );
        await queryInterface.bulkInsert("rolemappings", rolemappings, {});
      } catch (err) {
        console.log(err);
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async () => {
      await queryInterface.bulkDelete("rolemappings", null, {});
      await queryInterface.bulkDelete("roles", null, {});
      await queryInterface.bulkDelete("questionanswers", null, {});
      await queryInterface.bulkDelete("choices", null, {});
      await queryInterface.bulkDelete("questions", null, {});
      await queryInterface.bulkDelete("labs", null, {});
      await queryInterface.bulkDelete("accounts", null, {});
    });
  }
};
