"use strict";
const Sequelize = require("sequelize");
const _ = require("lodash");

function createAssociations(models, model, associations) {
  for (let assc of associations) {
    if (assc.extra) {
      if (assc.extra.through && models[assc.extra.through]) {
        console.log(models[assc.extra.through]);
        assc.extra.through = models[assc.extra.through];
      }
      model[assc.with] = model[assc.type](models[assc.with], assc.extra);
    } else {
      model[assc.with] = model[assc.type](models[assc.with]);
    }
    console.log(model, assc.type, model[assc.with], assc.with);
  }
}

function sortByDependencies(models) {
  let _arr = [];
  for (let model of models) {
    let idx = _arr.findIndex(item => item.name === model.name);
    if (idx !== -1) continue;
    if (model.dependencies) {
      let dependencies = getDependenicesList(models, model.dependencies, _arr);
      dependencies.forEach(item => {
        _arr.push(models.find(model => model.name === item));
      });
      if (dependencies.length === 0) _arr.push(model);
      if (_arr.findIndex(val => val.name === model.name) === -1)
        _arr.push(model);
    } else {
      _arr.push(model);
    }
  }
  return _arr;
}

function getDependenicesList(models, _dependencies, inDependencies) {
  let dependencies = [];
  for (let name of _dependencies) {
    if (inDependencies.findIndex(model => model.name === name) !== -1) continue;
    let model = models.find(val => val.name === name);
    if (model.dependencies) {
      getDependenicesList(models, model.dependencies, inDependencies).forEach(
        item => {
          if (dependencies.findIndex(name => name === item) === -1)
            dependencies.push(item);
        }
      );
    }
    if (dependencies.findIndex(name => name === model.name) === -1)
      dependencies.push(name);
  }
  return dependencies.filter(
    item => inDependencies.findIndex(dep => dep.name === item) === -1
  );
}

module.exports = {
  injectModel: (sequelize, modelConfigs) => {
    const models = {};
    for (let modelName of Object.keys(modelConfigs)) {
      const _config = modelConfigs[modelName](Sequelize.DataTypes);
      let model = sequelize.define(_config.name, _config.fields, _config.extra);
      models[modelName] = model;
    }

    for (let modelName of Object.keys(modelConfigs)) {
      const _config = modelConfigs[modelName](Sequelize.DataTypes);
      if (_config.associations && _config.associations.length > 0) {
        createAssociations(models, models[modelName], _config.associations);
      }
    }
    return models;
  },
  migrateServiceModels: async (
    service,
    modelConfigs,
    queryInterface,
    Sequelize
  ) => {
    try {
      let models = [];
      for (let config of Object.keys(modelConfigs)) {
        if (!_.isFunction(modelConfigs[config])) continue;
        const modelConfig = modelConfigs[config](Sequelize);
        if (modelConfig.extra && modelConfig.extra.timestamps) {
          modelConfig.fields.updatedAt = {
            type: Sequelize.DATE,
            allowNull: true
          };
          modelConfig.fields.createdAt = {
            type: Sequelize.DATE,
            allowNull: true
          };
        }
        models.push(modelConfig);
      }
      let sortModels = sortByDependencies(models);
      for (let model of sortModels) {
        try {
          await queryInterface.createTable(
            model.table,
            model.fields,
            model.extra || {}
          );
        } catch (err) {
          console.log(model);
          console.log(err);
          throw err;
        }
      }
    } catch (err) {
      console.log(err);
    }
  }
};
