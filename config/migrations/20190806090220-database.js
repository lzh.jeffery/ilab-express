"use strict";
const Models = require("../models");
const { migrateServiceModels } = require("../utils");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await migrateServiceModels("ilib", Models, queryInterface, Sequelize);
    return true;
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropAllTables();
  }
};
