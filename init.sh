#!/bin/bash
MYSQL_ROOT_PASS="local"
MYSQL_PORT=3306
DOCKER_CONTAINER="express-mysql"

while getopts "e:p:" arg
do
  case $arg in
    e)
      MYSQL_ROOT_PASS=${OPTARG#*=}
      ;;
    p)
      MYSQL_PORT=$OPTARG
      ;;
    ?)  
      exit 1
      ;;
    esac
done

rm -rf mysql

mkdir mysql

docker run -d --name $DOCKER_CONTAINER -p $MYSQL_PORT:3306 -v $PWD/mysql:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASS -v $PWD/scripts/local.databases.sql:/docker-entrypoint-initdb.d/local.databases.sql mysql:5.7 mysqld --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci

