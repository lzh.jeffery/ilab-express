DBHOST="localhost"
DBUSER="root"
DBPASS="local"

while getopts "h:p:u:i" arg
do
  case $arg in
    h)
      DBHOST=$OPTARG
      ;;
    p)
      DBPASS=$OPTARG
      ;;
    u)
      DBUSER=$OPTARG
      ;;
    i)
      cd config && DBPASS=$DBPASS npx sequelize db:migrate:undo && cd ../
      ;; 
    ?)  
      exit 1
      ;;
    esac
done

cd config && DBPASS=$DBPASS npx sequelize db:migrate && NODE_ENV=development DBPASS=$DBPASS npx sequelize db:seed:all