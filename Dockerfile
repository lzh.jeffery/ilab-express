FROM node:10.15.3-jessie

COPY npmrc /root/.npmrc

RUN echo "Asia/Shanghai" > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata

RUN mkdir /app
WORKDIR /app

COPY package.json ./

RUN yarn install
RUN yarn global add pm2
RUN pm2 install typescript

COPY . .

# RUN apt-get update && apt-get install -y vim

ENTRYPOINT [ "sh",  "./entrypoint.sh" ]
