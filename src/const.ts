const { ANSWER_SHEET_ITEM_TYPES, QUESTION_TYPES } = require("../config/models");

export const JWT = {
  SecretKey: "ilab_JWT_SECRET_KEY",
  TTL: 60 * 60 * 10
};

export const MODEL = {
  ANSWER_SHEET_ITEM_TYPES: {
    choice: ANSWER_SHEET_ITEM_TYPES[0],
    text: ANSWER_SHEET_ITEM_TYPES[1]
  },
  QUESTION_TYPES: {
    Radio: QUESTION_TYPES[0],
    MultipleSelection: QUESTION_TYPES[1],
    OpenQuestion: QUESTION_TYPES[2]
  }
};
