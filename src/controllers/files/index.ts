import {
  EResponse,
  ERequest,
  NextFunc,
  IError
} from "../../../const/interfaces";
import { upload } from "../../middlewares/formidable";
import DbService from "../../middlewares/DB";
import * as _ from "lodash";
import * as fs from "fs";
import * as path from "path";

export const createOne = async (
  req: ERequest,
  res: EResponse,
  next: NextFunc
) => {
  let transaction = await DbService.db.transaction();
  try {
    const file_msg: any = await upload(req);
    let fields = _.pick(file_msg.fields, ["type"]);
    let files = [];
    const AUDIO_PATH = path.join(__dirname, "../../../public/audios");
    if (!fs.existsSync(AUDIO_PATH)) fs.mkdirSync(AUDIO_PATH);
    for (let key in file_msg.files) {
      let file: any = file_msg.files[key];
      let Ext = file.name.slice(file.name.lastIndexOf("."));
      let _path = `${AUDIO_PATH}/${file.hash}${Ext}`;
      let fileObj = Object.assign(fields, {
        url: `/audios/${file.hash}${Ext}`,
        hash: file.hash
      });
      files.push(fileObj);
      fs.renameSync(file.path, _path);
      if (fs.existsSync(file.path)) fs.unlinkSync(file.path);
    }

    const _files = await DbService.models.file.bulkCreate(files, {
      transaction
    });

    await transaction.commit();
    return _files;
  } catch (err) {
    await transaction.rollback();
    throw new IError(err.message, 400);
  }
};
