import * as express from "express";
import ctrlWrapper from "../../../middlewares/controllerWrapper";
import * as fileCtrl from "../index";
const router = express.Router();

router.post("/", (req, res, next) =>
  ctrlWrapper(req, res, next, fileCtrl.createOne)
);

export default router;
