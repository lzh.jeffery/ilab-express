import { ERequest, EResponse, NextFunc } from "const/interfaces";
import DbService from "../../middlewares/DB";
import { get } from "../../middlewares/curd";

export const findById = async (
  req: ERequest,
  res: EResponse,
  next: NextFunc
) => {
  const ID = parseInt(req.params.id, 10);
  const result = await get(ID, req, DbService.models.lab);
  return result;
};
