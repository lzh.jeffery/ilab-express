import * as Joi from "@hapi/joi";

export const lab = {
  findById: {
    params: Joi.object()
      .keys({
        id: Joi.number()
          .positive()
          .required()
      })
      .required(),
    query: Joi.object().keys({
      filters: Joi.object().keys({
        include: Joi.array().items()
      })
    })
  }
};
