import * as express from "express";
import ctrlWrapper from "../../../middlewares/controllerWrapper";
import * as labCtrl from "../";
import { lab } from "../schemas";
const router = express.Router();

router.get("/:id", (req, res, next) =>
  ctrlWrapper(req, res, next, labCtrl.findById, {
    schema: lab.findById
  })
);

export default router;
