import * as express from "express";
import * as accountCtrl from "../";
import ctrlWrapper from "../../../middlewares/controllerWrapper";
import { accounts } from "../schemas/";
const router: express.Router = express.Router();

router.post("/login", (req, res, next) =>
  ctrlWrapper(req, res, next, accountCtrl.login, {
    schema: accounts.login
  })
);

router.delete("/:id", (req, res, next) =>
  ctrlWrapper(req, res, next, accountCtrl.delById, {
    schema: accounts.remove
  })
);

router.put("/:id", (req, res, next) =>
  ctrlWrapper(req, res, next, accountCtrl.updateById, {
    schema: accounts.update
  })
);

router.get("/:id", (req, res, next) =>
  ctrlWrapper(req, res, next, accountCtrl.findById, {
    schema: accounts.get
  })
);

router.post("/", (req, res, next) =>
  ctrlWrapper(req, res, next, accountCtrl.registered, {
    schema: accounts.registered
  })
);

router.get("/", (req, res, next) =>
  ctrlWrapper(req, res, next, accountCtrl.findAll, {
    schema: accounts.list
  })
);

export default router;
