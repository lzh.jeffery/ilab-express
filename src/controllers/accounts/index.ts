import DbService from "../../middlewares/DB";
import * as jsonwebtoken from "jsonwebtoken";
import * as bcrypt from "bcrypt";
import { Base64 } from "js-base64";
import * as _ from "lodash";
import { JWT } from "../../const";
import {
  ERequest,
  EResponse,
  NextFunc,
  IError
} from "../../../const/interfaces";
import { FindAndCountOptions, FindOptions } from "sequelize";
import { list, get, update, remove, create } from "../../middlewares/curd";
const QUERY_ATTRIBUTES_EXCLUDE = ["password"];
const queryOptions: FindAndCountOptions | FindOptions = {
  attributes: {
    exclude: QUERY_ATTRIBUTES_EXCLUDE
  }
};

export const registered = async (
  req: ERequest,
  res: EResponse,
  next: NextFunc
) => {
  let KEYS = ["name", "identity", "password", "phone", "lesson_id"];
  let accountData = _.pick(req.body, KEYS);
  accountData.username = accountData.name;
  accountData.isVerified = null;
  const ROLE = req.body.role;
  let transaction = await DbService.db.transaction();
  try {
    if (accountData.lesson_id) {
      let lesson = await DbService.models.lesson.findOne({
        where: {
          id: accountData.lesson_id
        }
      });
      if (!lesson || !lesson.isActive) throw new IError("班级信息错误", 400);
    }

    let role = await DbService.models.role.findOne({
      where: {
        name: ROLE
      }
    });
    if (!role) throw new IError("Role not exist", 422);

    accountData.password = bcryptHashPassword(accountData.password);

    let account = await DbService.models.account.findOne({
      where: {
        identity: accountData.identity
      }
    });
    console.log(account);
    if (account && account.isVerified) {
      throw new IError("用户已经被注册", 400);
    }
    let result;
    if (account) {
      result = await DbService.models.account.update(
        Object.assign(accountData, {
          isVerified: null
        }),
        {
          where: {
            id: account.id
          },
          transaction
        }
      );
    } else {
      result = await create(accountData, DbService.models.account, {
        transaction
      });

      let rolemapping = {
        account_id: result.id,
        role_id: role.id
      };
      await DbService.models.rolemappings.create(rolemapping, {
        transaction
      });
    }

    await transaction.commit();

    delete result.dataValues.password;
    delete result.dataValues.isVerified;
    return result.dataValues;
  } catch (err) {
    await transaction.rollback();
    throw err;
  }
};

export const findAll = async (
  req: ERequest,
  res: EResponse,
  next: NextFunc
) => {
  return await list(req, DbService.models.account, queryOptions);
};

export const findById = async (req: ERequest) => {
  return get(req.params.id, req, DbService.models.account, queryOptions);
};

export const updateById = async (req: ERequest) => {
  if (req.body.password) {
    let UID = req.meta ? req.meta.user.id || null : null;
    let ROLE = req.meta ? req.meta.role : null;
    if (UID !== req.params.id && ROLE !== "SYSTEM_ADMIN")
      throw new IError("没有权限更新用户密码", 400);
    req.body.password = bcryptHashPassword(req.body.password);
  }
  return update(
    req.params.id,
    req.body,
    DbService.models.account,
    Object.assign({ where: {} }, queryOptions)
  );
};

export const delById = async (req: ERequest) => {
  return remove(req.params.id, DbService.models.account);
};

export const login = async (req: ERequest) => {
  let accountMsg = _.pick(req.body, ["identity"]);
  let account = await DbService.models.account.findOne({
    where: accountMsg,
    include: ["roles", "lesson"]
  });
  if (!account) throw new IError("工号或学号尚未被注册", 400);
  if (account.isVerified === null || account.isVerified === false)
    throw new IError("用户未被通过验证", 400);
  if (!bcryptHashCompare(req.body.password, account.password))
    throw new IError("账号密码不正确", 400);
  let sign = signToken(account.dataValues);
  return Object.assign(
    {
      user: account
    },
    sign
  );
};

// utils
export const signToken = (user: any) => {
  QUERY_ATTRIBUTES_EXCLUDE.forEach(key => {
    if (user[key]) delete user[key];
  });
  let token = jsonwebtoken.sign({ user: JSON.stringify(user) }, JWT.SecretKey, {
    expiresIn: "10h"
  });
  return {
    expires: Number(new Date()) + JWT.TTL * 1000,
    token
  };
};

function bcryptHashPassword(pwd: string) {
  let _pwd = Base64.decode(pwd);
  let salt = bcrypt.genSaltSync(10);
  return bcrypt.hashSync(_pwd, salt);
}

function bcryptHashCompare(pwd: string, bcryptPass: string) {
  let _pwd = Base64.decode(pwd);
  return bcrypt.compareSync(_pwd, bcryptPass);
}
