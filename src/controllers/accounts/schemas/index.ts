import * as Joi from "@hapi/joi";

const Account = {
  id: Joi.number().positive(),
  username: Joi.string()
    .min(1)
    .max(50),
  name: Joi.string()
    .min(1)
    .max(100),
  phone: Joi.string().length(11),
  identity: Joi.string().regex(/^[0-9|a-z|A-Z]*$/, "identity"),
  password: Joi.string()
};

export const accounts = {
  list: {
    query: Joi.object().keys({
      filters: Joi.object().keys({
        include: Joi.array(),
        order: Joi.array(),
        page: Joi.number().positive(),
        size: Joi.number().positive(),
        where: Joi.object()
      })
    })
  },
  get: {
    params: Joi.object()
      .keys({
        id: Account.id.required()
      })
      .required(),
    query: Joi.object().keys({
      filters: Joi.object().keys({
        include: Joi.array()
      })
    })
  },
  update: {
    params: Joi.object()
      .keys({
        id: Account.id.required()
      })
      .required(),
    body: Joi.object()
      .keys({
        username: Account.username,
        phone: Account.phone.regex(/[0-9]{11}/, "phone number"),
        isVerified: Joi.boolean(),
        password: Account.password
      })
      .required()
  },
  remove: {
    params: Joi.object()
      .keys({
        id: Account.id.required()
      })
      .required()
  },
  login: {
    body: Joi.object()
      .keys({
        identity: Account.identity
          .allow(["ROOT", "STUDENT", "TEACHER"])
          .required(),
        password: Joi.string().required()
      })
      .required()
  },
  registered: {
    body: Joi.object()
      .keys({
        name: Account.name.required(),
        phone: Account.phone.required(),
        password: Joi.string()
          .min(5)
          .required(),
        identity: Joi.string()
          .regex(/^[0-9|a-z|A-Z]*$/, "identity")
          .required(),
        role: Joi.string()
          .allow(["TEACHER", "STUDENT"])
          .required(),
        lesson_id: Joi.number().positive()
      })
      .required()
  }
};
