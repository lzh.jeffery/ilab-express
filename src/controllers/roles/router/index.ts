import * as express from "express";
import ctrlWrapper from "../../../middlewares/controllerWrapper";
import * as roleCtrl from "../";
import { role } from "../schemas";
const router = express.Router();

router.get("/", (req, res, next) =>
  ctrlWrapper(req, res, next, roleCtrl.findAll, {
    schema: role.list
  })
);

export default router;
