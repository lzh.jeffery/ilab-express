import * as Joi from "@hapi/joi";

export const role = {
  list: {
    query: Joi.object().keys({
      filters: Joi.object().keys({
        page: Joi.number().positive(),
        size: Joi.number().positive(),
        where: Joi.object()
      })
    })
  }
};
