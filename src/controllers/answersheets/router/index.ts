import * as express from "express";
import ctrlWrapper from "../../../middlewares/controllerWrapper";
import * as answersheetCtrl from "../";
import { answersheet } from "../schemas";
const router = express.Router();

router.put("/:id", (req, res, next) =>
  ctrlWrapper(req, res, next, answersheetCtrl.updateById, {
    schema: answersheet.update
  })
);

router.get("/:id", (req, res, next) =>
  ctrlWrapper(req, res, next, answersheetCtrl.findById, {
    schema: answersheet.get
  })
);

router.post("/", (req, res, next) =>
  ctrlWrapper(req, res, next, answersheetCtrl.createOne, {
    schema: answersheet.create
  })
);

router.get("/", (req, res, next) =>
  ctrlWrapper(req, res, next, answersheetCtrl.findAll, {
    schema: answersheet.list
  })
);

router.post("/pass",(req,res,next) => 
  ctrlWrapper(req, res, next, answersheetCtrl.expertPass, {
    schema: answersheet.create
  })
);

export default router;
