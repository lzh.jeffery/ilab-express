import * as Joi from "@hapi/joi";
import { MODEL } from "../../../const";

const AnswerSheet = {
  id: Joi.number().positive(),
  lab_id: Joi.number().positive(),
  account_id: Joi.number().positive(),
  total: Joi.number(),
  score: Joi.number(),
  isFinish: Joi.boolean(),
  isEnd: Joi.boolean(),
  vr: Joi.number()
};

const AnswerSheetItem = {
  id: Joi.number().positive(),
  answersheet_id: AnswerSheet.id,
  question_id: Joi.number().positive(),
  type: Joi.string().valid(Object.values(MODEL.ANSWER_SHEET_ITEM_TYPES)),
  score: Joi.number().positive(),
  data: Joi.string(),
  comment: Joi.string()
};

export const answersheet = {
  create: {
    body: Joi.object()
      .keys({
        lab_id: AnswerSheet.lab_id.required(),
        vr: AnswerSheet.vr,
        items: Joi.array()
          .items(
            Joi.object()
              .keys({
                question_id: Joi.number().required(),
                choices: Joi.array().items(
                  Joi.number()
                    .positive()
                    .required()
                ),
                text: Joi.string()
              })
              .required()
          )
          .required()
        // audios: Joi.array().items(
        //   Joi.object()
        //     .keys({
        //       file_id: Joi.number().required(),
        //       seq: Joi.number().required()
        //     })
        //     .required()
        // )
      })
      .required()
  },
  list: {
    query: Joi.object().keys({
      filters: Joi.object().keys({
        page: Joi.number().positive(),
        size: Joi.number().positive(),
        where: Joi.object(),
        include: Joi.array(),
        order: Joi.array()
      })
    })
  },
  update: {
    params: Joi.object()
      .keys({
        id: AnswerSheet.id.required()
      })
      .required(),
    body: Joi.object()
      .keys({
        answerSheetItems: Joi.array()
          .items(
            Joi.object()
              .keys({
                id: AnswerSheetItem.id.required(),
                comment: AnswerSheetItem.comment.required(),
                score: AnswerSheetItem.score.required()
              })
              .required()
          )
          .required()
      })
      .required()
  },
  get: {
    params: Joi.object()
      .keys({
        id: AnswerSheet.id.required()
      })
      .required(),
    query: Joi.object().keys({
      filters: Joi.object().keys({
        page: Joi.number().positive(),
        size: Joi.number().positive(),
        include: Joi.array()
      })
    })
  }
};
