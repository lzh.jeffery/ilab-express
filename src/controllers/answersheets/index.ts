import {
  ERequest,
  EResponse,
  NextFunc,
  IError
} from "../../../const/interfaces";
import DbService from "../../middlewares/DB";
import { Op } from "sequelize";
import { create, list, update, get } from "../../middlewares/curd";
import { MODEL } from "../../const";
import * as _ from "lodash";
const { QUESTION_TYPES } = require("../../../config/models/index.js");

export const createOne = async (
  req: ERequest,
  res: EResponse,
  next: NextFunc
) => {
  let transaction = await DbService.db.transaction();
  try {
    // 校验用户
    const UID = req.meta ? req.meta.user.id : null;
    const LID = req.meta ? req.meta.user.lesson_id : null;
    if (!UID) throw new IError("用户信息错误", 400);
    // 校验实验信息
    const lab = await DbService.models.lab.findByPk(req.body.lab_id, {
      include: ["questions"]
    });
    if (lab === null) throw new IError("实验信息错误", 400);
    if (req.body.vr && lab.vrScore < req.body.vr)
      throw new IError("VR视频分数不合法", 400);
    // 初始化数据
    let answersheetData: any = {
      lab_id: req.body.lab_id,
      lesson_id: LID,
      account_id: UID,
      total: lab.vrScore || 0,
      isEnd:
        lab.questions.findIndex(
          (question: any) => question.type === QUESTION_TYPES[2]
        ) === -1
    };
    lab.questions.forEach((question: any) => {
      answersheetData.total += question.score;
    });
    let questionIds = req.body.items
      .map((item: any) => item.question_id)
      .filter((val: any) => val !== undefined);
    let _questionsData = await DbService.models.question.findAll({
      where: {
        id: {
          [Op.or]: questionIds
        }
      },
      include: ["answers", "choices"]
    });
    answersheetData.score = req.body.vr || 0;
    // 计算得分
    const { score, answersheetitems } = statisticsQuestionMessage(
      _questionsData,
      req.body.items
    );
    answersheetData.score += score;
    const result = await create(answersheetData, DbService.models.answersheet, {
      transaction
    });

    // if (req.body.audios) {
    //   await DbService.models.answersheetaudioitem.bulkCreate(
    //     req.body.audios.map((_val: any) => {
    //       return {
    //         file_id: _val.file_id,
    //         seq: _val.seq,
    //         answersheet_id: result.id
    //       };
    //     }),
    //     {
    //       transaction
    //     }
    //   );
    // }

    await DbService.models.answersheetitem.bulkCreate(
      answersheetitems.map((val: any) => {
        val.answersheet_id = result.id;
        return val;
      }),
      {
        transaction
      }
    );
    await transaction.commit();
    return result;
  } catch (err) {
    await transaction.rollback();
    throw err;
  }
};

export const expertPass = async (
  req: ERequest,
  res: EResponse,
  next: NextFunc
) => {
  try {
    const lab = await DbService.models.lab.findByPk(req.body.lab_id, {
      include: ["questions"]
    });
    if (lab === null) throw new IError("实验信息错误", 400);
    // 初始化数据
    let answersheetData: any = {
      lab_id: req.body.lab_id,
      total: 0,
      isEnd:
        lab.questions.findIndex(
          (question: any) => question.type === QUESTION_TYPES[2]
        ) === -1
    };
    lab.questions.forEach((question: any) => {
      answersheetData.total += question.score;
    });
    let questionIds = req.body.items
      .map((item: any) => item.question_id)
      .filter((val: any) => val !== undefined);
    let _questionsData = await DbService.models.question.findAll({
      where: {
        id: {
          [Op.or]: questionIds
        }
      },
      include: ["answers", "choices"]
    });
    answersheetData.isFinish = _questionsData.length === lab.questions.length;
    answersheetData.score = 0;
    // 计算得分
    const { score, answersheetitems } = statisticsQuestionMessage(
      _questionsData,
      req.body.items
    );
    answersheetData.answers = answersheetitems;
    answersheetData.score = score;
    return answersheetData;
  } catch (err) {
    throw err;
  }
};

export const findAll = async (
  req: ERequest,
  res: EResponse,
  next: NextFunc
) => {
  return await list(req, DbService.models.answersheet);
};

export const updateById = async (
  req: ERequest,
  res: EResponse,
  next: NextFunc
) => {
  const ID = req.params.id;
  const { answerSheetItems } = req.body;
  let transaction = await DbService.db.transaction();
  try {
    let answerSheet = await DbService.models.answersheet.findByPk(ID, {
      include: [
        {
          association: "answers",
          include: ["question"]
        }
      ]
    });
    if (!answerSheet) throw new IError(`Request lab with id=${ID} not exist`);
    let score = answerSheet.score;
    let _answerSheetItems = answerSheetItems.filter((val: any) => {
      let answerSheetItem = answerSheet.answers.find(
        (_val: any) => _val.id === val.id
      );
      if (
        !answerSheetItem ||
        answerSheetItem.type !== MODEL.ANSWER_SHEET_ITEM_TYPES.text
      )
        return false;
      if (answerSheetItem.question.score < val.score)
        throw new IError("答题评分大于题目分值", 422);
      return true;
    });
    // check is End
    let notReviewed = answerSheet.answers.filter(
      (val: any) =>
        val.type === MODEL.ANSWER_SHEET_ITEM_TYPES.text && val.comment === null
    );

    let items: Array<any> = [];
    for (let sheetitem of _answerSheetItems) {
      const result = await update(
        sheetitem.id,
        _.pick(sheetitem, ["score", "comment"]),
        DbService.models.answersheetitem,
        {
          where: {},
          transaction
        }
      );
      let item = answerSheet.answers.find((ans: any) => ans.id === result.id);
      score -= item.score;
      score += result.score;
      let notReviewIdx = notReviewed.findIndex(
        (val: any) => val.id === result.id
      );
      if (notReviewIdx !== -1) notReviewed.splice(notReviewIdx, 1);

      items.push(result);
    }

    await DbService.models.answersheet.update(
      { score: score, isEnd: notReviewed.length === 0 },
      {
        where: {
          id: ID
        },
        transaction
      }
    );

    await transaction.commit();
    return items;
  } catch (err) {
    transaction.rollback();
    throw err;
  }
};

export const findById = async (
  req: ERequest,
  res: EResponse,
  next: NextFunc
) => {
  return await get(req.params.id, req, DbService.models.answersheet);
};

/**
 *
 * @param questionsData Array<any>
 */
function statisticsQuestionMessage(
  questionsData: Array<any>,
  originDatas: Array<any>
) {
  console.log(questionsData.map(val => val.dataValues));
  let answersheetitems: Array<any> = [];
  let score = 0;
  questionsData.forEach(question => {
    let answersheetitem: any = {
      question_id: question.id,
      question: question,
      type: MODEL.ANSWER_SHEET_ITEM_TYPES.choice,
      score: 0
    };
    if (question.type === QUESTION_TYPES[2]) {
      // 处理开放性问题
      let item = originDatas.find(
        (val: any) => val.question_id === question.id
      );
      answersheetitem.data = item.text;
      answersheetitem.type = MODEL.ANSWER_SHEET_ITEM_TYPES.text;
      answersheetitems.push(answersheetitem);
      return;
    }
    let sure = false;
    let item = originDatas.find((val: any) => val.question_id === question.id);
    let _choices = [].concat(item.choices);

    // 处理选择题
    let sures = [].concat(question.answers);
    sure = sures.every((_val: any) => {
      let idx = _choices.findIndex((_id: number) => _id === _val.id);
      if (idx !== -1) {
        _choices.splice(idx, 1);
      }
      return idx !== -1;
    });
    answersheetitem.score = 0;
    answersheetitem.data = JSON.stringify({
      sures: sures,
      answers: question.choices.filter(
        (ch: any) => item.choices.findIndex((id: number) => id === ch.id) !== -1
      )
    });

    answersheetitems.push(answersheetitem);
    if (!sure || _choices.length !== 0) return;
    answersheetitem.score = question.score;
    // switch (question.type) {
    //   case QUESTION_TYPES[0]:
    //     if (!sure || _choices.length !== 0) return;
    //     answersheetitem.score = question.score;
    //     break;
    //   case QUESTION_TYPES[1]:
    //     if (!sure || _choices.length !== 0) return;
    //     if (sure && _choices.length === 0) {
    //       answersheetitem.score = question.score;
    //     }
    //     break;
    // }
    score += question.score;
  });
  return {
    score,
    answersheetitems
  };
}
