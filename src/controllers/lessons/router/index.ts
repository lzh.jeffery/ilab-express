import * as express from "express";
import * as lessonCtrl from "../";
import { lesson } from "../schemas";
import ctrlWrapper from "../../../middlewares/controllerWrapper";
const router = express.Router();

router.get("/", (req, res, next) =>
  ctrlWrapper(req, res, next, lessonCtrl.findAll, {
    schema: lesson.list
  })
);

export default router;
