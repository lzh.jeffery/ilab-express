import { ERequest, EResponse, NextFunc } from "../../../const/interfaces";
import DbService from "../../middlewares/DB";
import { list } from "../../middlewares/curd";

export const findAll = async (
  req: ERequest,
  res: EResponse,
  next: NextFunc
) => {
  return await list(req, DbService.models.lesson, {
    attributes: {
      exclude: ["isActive"]
    },
    where: {
      isActive: true
    }
  });
};
