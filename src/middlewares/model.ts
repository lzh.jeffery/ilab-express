import * as Sequelize from "sequelize";
import * as _ from "lodash";

function createAssociations(models: any, model: any, associations: any) {
  for (let assc of associations) {
    if (assc.extra) {
      if (assc.extra.through && models[assc.extra.through]) {
        console.log(models[assc.extra.through]);
        assc.extra.through = models[assc.extra.through];
      }
      model[assc.with] = model[assc.type](models[assc.with], assc.extra);
    } else {
      model[assc.with] = model[assc.type](models[assc.with]);
    }
    console.log(model, assc.type, model[assc.with], assc.with);
  }
}

export default function(sequelize: any, modelConfigs: any) {
  const models: any = {};
  for (let modelName of Object.keys(modelConfigs)) {
    if (!_.isFunction(modelConfigs[modelName])) continue;
    const _config = modelConfigs[modelName](Sequelize.DataTypes);
    let model = sequelize.define(_config.name, _config.fields, _config.extra);
    models[modelName] = model;
  }

  for (let modelName of Object.keys(modelConfigs)) {
    if (!_.isFunction(modelConfigs[modelName])) continue;
    const _config = modelConfigs[modelName](Sequelize.DataTypes);
    if (_config.associations && _config.associations.length > 0) {
      createAssociations(models, models[modelName], _config.associations);
    }
  }
  return models;
}
