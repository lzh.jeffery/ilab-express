import { ERequest, EResponse, NextFunc, IError } from "../../const/interfaces";
import * as createError from "http-errors";

interface Options {
  schema?: any;
}

function validate(validatorSchema: any, validateData: any) {
  const { error, value } = validatorSchema.validate(validateData);
  if (error !== null) throw error;
  return value;
}

function validatorSchema(key: string, validator: any, req: ERequest) {
  try {
    let res;
    switch (key) {
      case "body":
        res = validate(validator.body, req.body);
        req.body = res;
        break;
      case "query":
        res = validate(validator.query, req.query);
        req.query = res;
        break;
      case "params":
        res = validate(validator.params, req.params);
        req.params = res;
        break;
    }
  } catch (err) {
    throw err;
  }
}

export default async (
  req: ERequest | any,
  res: EResponse,
  next: NextFunc,
  func: (req: ERequest, res: EResponse, next: NextFunc) => {},
  options: Options = {}
) => {
  try {
    if (options.schema !== null) {
      const Schemas = options.schema;
      let KEYS = ["query", "params", "body"];
      for (let key of KEYS) {
        if (req[key] && Object.keys(req[key]).length !== 0) {
          if (Schemas[key]) validatorSchema(key, Schemas, req);
          else throw new IError("存在未定义参数", 422);
        }
      }
    }
    const results = await func(req, res, next);
    return res.send(results);
  } catch (err) {
    if (process.env.NODE_ENV === "development") console.error(err);
    return next(createError(err));
  }
};
