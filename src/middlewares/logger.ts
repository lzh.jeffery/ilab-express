import * as fs from "fs";
import * as logger from "morgan";
import * as path from "path";
const rfs = require("rotating-file-stream");

let logDirectory = path.join(__dirname, "../../logs");
if (!fs.existsSync(logDirectory)) fs.mkdirSync(logDirectory);

let accessLogStream = rfs(getFileName(), {
  interval: "1h",
  path: logDirectory
});

function getFileName() {
  return `${new Date(new Date().setMinutes(0, 0, 0)).toLocaleString()}.log`;
}

export default logger(
  function(tokens, req, res) {
    return [
      tokens["remote-addr"](req, res),
      "-",
      "-",
      `[${new Date().toLocaleString()}] -`,
      tokens.method(req, res),
      tokens.url(req, res),
      tokens.status(req, res),
      tokens.res(req, res, "content-length"),
      "-",
      tokens["response-time"](req, res),
      "ms",
      "- data",
      `params: ${JSON.stringify(req.params)}`,
      `query: ${JSON.stringify(req.query)}`,
      `body: ${JSON.stringify(req.body)}`,
      `- user-agent: ${req.headers["user-agent"]}`
    ].join(" ");
  },
  { stream: accessLogStream }
);
