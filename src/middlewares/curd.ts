import * as _ from "lodash";
import { ERequest, IError } from "../../const/interfaces";
import { Model } from "./DB";
import {
  FindOptions,
  UpdateOptions,
  DestroyOptions,
  CreateOptions
} from "sequelize/types";
import { Op } from "sequelize";

const operatorsAliases: any = {
  $eq: Op.eq,
  $ne: Op.ne,
  $gte: Op.gte,
  $gt: Op.gt,
  $lte: Op.lte,
  $lt: Op.lt,
  $not: Op.not,
  $in: Op.in,
  $notIn: Op.notIn,
  $is: Op.is,
  $like: Op.like,
  $notLike: Op.notLike,
  $iLike: Op.iLike,
  $notILike: Op.notILike,
  $regexp: Op.regexp,
  $notRegexp: Op.notRegexp,
  $iRegexp: Op.iRegexp,
  $notIRegexp: Op.notIRegexp,
  $between: Op.between,
  $notBetween: Op.notBetween,
  $overlap: Op.overlap,
  $contains: Op.contains,
  $contained: Op.contained,
  $adjacent: Op.adjacent,
  $strictLeft: Op.strictLeft,
  $strictRight: Op.strictRight,
  $noExtendRight: Op.noExtendRight,
  $noExtendLeft: Op.noExtendLeft,
  $and: Op.and,
  $or: Op.or,
  $any: Op.any,
  $all: Op.all,
  $values: Op.values,
  $col: Op.col
};

function setUpPageAndSize(filters: any, options: FindOptions) {
  const PAGE = filters.page || undefined;
  const SIZE = filters.size || undefined;
  if (SIZE) options.limit = SIZE;
  if (PAGE && SIZE) options.offset = (PAGE - 1) * SIZE;
}

function setUpWhere(filters: any) {
  let WHERE: any = {};
  let where = filters.where || {};
  WHERE = Object.assign(where, {});

  for (let attribute in where) {
    if (_.isObject(where[attribute])) {
      for (let SequelizeKey in where[attribute]) {
        if (operatorsAliases[SequelizeKey]) {
          WHERE[attribute][operatorsAliases[SequelizeKey]] =
            where[attribute][SequelizeKey];
        } else {
          WHERE[attribute][SequelizeKey] = where[attribute][SequelizeKey];
        }
      }
    }
  }

  return WHERE;
}

function setUpInclude(filters: any) {
  let INCLUDE: Array<any> = [];
  let include: Array<any> = filters.include || [];

  INCLUDE = include.map(inc => {
    if (inc.where) inc.where = setUpInclude(inc);
    return inc;
  });

  return INCLUDE;
}

function setUpOrder(filters: any): any {
  let ORDER: Array<any> = [];
  let order: Array<any> = filters.order || [];

  ORDER = order.map(o => {
    let arr: Array<any> = [];
    arr.push(o.key);
    arr.push(o.sort);
    return arr;
  });
  return ORDER;
}

function optionsBuilder(query: any, options: FindOptions = {}): FindOptions {
  const FILTERS = query.filters ? query.filters : {};

  setUpPageAndSize(FILTERS, options);

  let where = setUpWhere(FILTERS);
  if (Object.keys(where).length > 0) {
    if (!options.where) options.where = {};
    options.where = Object.assign(options.where, where);
  }

  let include = setUpInclude(FILTERS);
  if (include.length > 0) {
    if (!options.include) options.include = [];
    options.include = options.include.concat(include);
  }

  let order = setUpOrder(FILTERS);
  if (order.length > 0) {
    if (!options.order) {
      options.order = [];
      options.order = options.order.concat(order);
    }
  }

  return options;
}

export const list = async (
  req: ERequest,
  model: Model<any>,
  options?: FindOptions
) => {
  try {
    let _options = optionsBuilder(req.query || {}, options || {});
    const result = await model.findAndCountAll(_options);

    return {
      total: result.count,
      items: result.rows
    };
  } catch (err) {
    throw err;
  }
};

export const get = async (
  id: number | string,
  req: ERequest,
  model: Model<any>,
  options?: FindOptions
) => {
  try {
    let _options = optionsBuilder(req.query, options || {});
    const result = await model.findByPk(id, _options);

    if (result === null) {
      let err = new IError(`request model with id=${id} not exist`, 404);
      throw err;
    }

    return result;
  } catch (err) {
    throw err;
  }
};

export const update = async (
  id: number | string,
  value: any,
  model: Model<any>,
  options: UpdateOptions = { where: {} }
) => {
  try {
    const result = await model.findByPk(id, options);

    if (result === null)
      throw new IError(`Request with id=${id} not exist`, 404);
    if (Object.keys(value).length === 0)
      throw new IError(`Update body not exist`, 400);
    return await result.update(value, options);
  } catch (err) {
    throw err;
  }
};

export const remove = async (
  id: number | string,
  model: Model<any>,
  options: DestroyOptions = {}
) => {
  try {
    const result = await model.findByPk(id, options);

    if (result === null) throw new IError(`Model with id=${id} not exist`, 404);

    return await result.destroy(options);
  } catch (err) {
    throw err;
  }
};

export const create = async (
  value: any,
  model: Model<any>,
  options: CreateOptions = {}
) => {
  try {
    return await model.create(value, options);
  } catch (err) {
    throw err;
  }
};
