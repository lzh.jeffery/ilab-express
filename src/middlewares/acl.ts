import { ERequest, EResponse, NextFunc, IError } from "../../const/interfaces";
import * as acl from "express-acl";
import * as jsonwebtoken from "jsonwebtoken";
import { JWT } from "../const";

const unlessPath: Array<any> = [
  "/",
  /^(?!\/api)/,
  /[(.jpg|.html|.css|.js|.png|.json|.unityweb)]$/,
  "/api/accounts/login",
  /^\/api\/roles/,
  "/api/lessons",
  /^\/api\/labs\/[0-9]*$/
];

acl.config({
  baseUrl: "api",
  filename: "nacl.yaml",
  defaultRole: "TOURIST",
  decodedObjectName: "meta"
});

export default {
  authorize: acl.authorize.unless({ path: unlessPath }),
  resolveToken: (req: ERequest, res: EResponse, next: NextFunc) => {
    const str: any =
      req.headers["authorization"] || req.headers["Authorization"];

    if (!str) {
      req.meta = {
        role: "TOURIST"
      };
      return next();
    }
    const [type, token] = str.split(" ");
    if (!token) {
      req.meta = {
        role: "TOURIST"
      };
      return next();
    }

    jsonwebtoken.verify(token, JWT.SecretKey, (err: Error, decoded: any) => {
      if (err) return next(new IError(err.message, 400));
      let _user = JSON.parse(decoded.user);
      req.meta = {
        role: _user.roles[0].name,
        user: _user
      };
      return next();
    });
  }
};
