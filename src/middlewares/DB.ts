import {
  Sequelize,
  FindAndCountOptions,
  FindOptions,
  CreateOptions,
  BulkCreateOptions,
  UpdateOptions
} from "sequelize";
import InjectModels from "./model";
const dbConfig = require("../../db.config.js");
const Models = require("../../config/models");

export interface Model<T> {
  findAll: (options?: FindOptions) => Promise<Array<T>>;
  findAndCountAll: (
    options?: FindAndCountOptions
  ) => Promise<{ count: number; rows: Array<T> }>;
  findByPk: (id: number | string, options?: FindOptions) => Promise<T>;
  create: (values: any, options?: CreateOptions) => Promise<T>;
  bulkCreate: (records: Array<any>, options?: BulkCreateOptions) => Promise<T>;
  update: (value: any, options: UpdateOptions) => Promise<T>;
  findOne: (options: FindOptions) => Promise<T>;
}

interface ModelsOption {
  lab: Model<any>;
  question: Model<any>;
  choice: Model<any>;
  questionanswer: Model<any>;
  account: Model<any>;
  role: Model<any>;
  rolemappings: Model<any>;
  answersheet: Model<any>;
  answersheetitem: Model<any>;
  lesson: Model<any>;
  file: Model<any>;
  // answersheetaudioitem: Model<any>;
}

class Db {
  private _db: Sequelize;
  private _models: ModelsOption;
  private _isConnect: boolean = false;

  get isConnect() {
    return this._isConnect;
  }

  get db() {
    return this._db;
  }

  get models() {
    return this._models;
  }

  constructor() {
    try {
      this._db = new Sequelize(
        dbConfig.database,
        dbConfig.user,
        dbConfig.password,
        {
          dialect: "mysql",
          port: parseInt(dbConfig.port, 10),
          host: dbConfig.host
        }
      );
      this._isConnect = true;
      this._models = InjectModels(this.db, Models);
    } catch (err) {
      throw err;
    }
    return this;
  }
}

export default new Db();
