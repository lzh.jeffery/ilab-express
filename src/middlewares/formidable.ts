import * as formidable from "formidable";
import { ERequest } from "../../const/interfaces";

export const upload = (req: ERequest) => {
  let form: formidable.IncomingForm = new formidable.IncomingForm();
  form.encoding = "utf-8";
  form.keepExtensions = true;
  form.maxFieldsSize = 20 * 1024 * 1024;
  form.maxFileSize = 20 * 1025 * 1024;
  form.hash = "md5";
  return new Promise((resolve, reject) => {
    form.parse(req, (err, fields, files) => {
      if (err) reject(err);
      return resolve({
        fields,
        files
      });
    });
  });
};
