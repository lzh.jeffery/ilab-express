import * as express from "express";
import * as createError from "http-errors";
import * as path from "path";
import * as cookieParser from "cookie-parser";
import * as logger from "morgan";
import * as cors from "cors";
import * as history from "connect-history-api-fallback";
import { ERequest, EResponse, NextFunc, IError } from "../const/interfaces";
import logs from "./middlewares/logger";
import * as fs from "fs";

const app: express.Express = express();

if (process.env.NODE_ENV === "production") {
  app.use(logs);
} else {
  app.use(logger("dev"));
  app.use(cors());
}
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

import ACL from "./middlewares/acl";
app.use(ACL.resolveToken);
app.use(ACL.authorize);

import labRouter from "./controllers/labs/router";
app.use("/api/labs", labRouter);
import accountRouter from "./controllers/accounts/router";
app.use("/api/accounts", accountRouter);
import answersheetRouter from "./controllers/answersheets/router";
app.use("/api/answersheets", answersheetRouter);
import roleRouter from "./controllers/roles/router";
app.use("/api/roles", roleRouter);
import lessonRouter from "./controllers/lessons/router";
app.use("/api/lessons", lessonRouter);
import fileRouter from "./controllers/files/router";
app.use("/api/files", fileRouter);

app.use(
  history({
    index: "index.html",
    verbose: true
  })
);

app.get("/index.html", function(req, res) {
  res.setHeader("Content-Type", "text/html");
  res.setHeader("Cache-Control", "public, max-age=0");

  let stream = fs.createReadStream(path.join(__dirname, "../dist/index.html"));
  stream.pipe(res);
});

app.use(
  express.static(path.join(__dirname, "../public"), {
    maxAge: process.env.NODE_ENV === "development" ? 0 : 1000 * 60 * 60 * 1
  })
);
app.use(
  express.static(path.join(__dirname, "../dist"), {
    maxAge: process.env.NODE_ENV === "development" ? 0 : 1000 * 60 * 60 * 1
  })
);

app.use((req, res, next) => {
  next(createError(404));
});

app.use((err: IError, req: ERequest, res: EResponse, next: NextFunc) => {
  if (err.name === "ValidationError") err.status = 422;
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  res.status(err.status || 5000);
  res.send({
    code: err.status,
    message: err.message
  });
});

module.exports = app;
