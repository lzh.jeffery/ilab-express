import * as express from "express";

export interface ERequest extends express.Request {
  meta?: {
    role?: string;
    user?: any;
  };
}
export interface EResponse extends express.Response {}
export interface NextFunc extends express.NextFunction {}
export interface IError extends Error {
  status: number;
}
export class IError extends Error {
  public status: number;
  constructor(errMsg: string, status?: number) {
    super(errMsg);
    this.status = status || 400;
    return this;
  }
}
