#!/bin/bash

# if [ ${MIGRATE_DB} = "true" ]
# then
#   cd ./init.sh -e MYSQL_ROOT_PASS=${MYSQL_PASS} -p ${MYSQL_PORT}

#   sleep 20
# fi

if [ ${SEED_DB} = "true" ]
then
  ./init.data.sh -p $MYSQL_PASS -u root -h mysql
fi

# mv ./dist/* ./public

pm2-docker start pm2.config.json
